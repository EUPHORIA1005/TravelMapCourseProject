create table categories
(
    id        serial primary key,
    category  varchar not null unique,
    parent_id int default (0)
);

INSERT INTO categories (category)
values ('eating establishment');
INSERT INTO categories (category)
values ('cultural attraction');

INSERT INTO categories (category, parent_id)
values ('cafe', 1);
INSERT INTO categories (category, parent_id)
values ('restaurant', 1);
INSERT INTO categories (category, parent_id)
values ('canteen', 1);
INSERT INTO categories (category, parent_id)
values ('bar', 1);

INSERT INTO categories (category, parent_id)
values ('museum', 2);
INSERT INTO categories (category, parent_id)
values ('gallery', 2);
INSERT INTO categories (category, parent_id)
values ('library', 2);
INSERT INTO categories (category, parent_id)
values ('cinema', 2);

create table attractions
(
    id          serial primary key,
    name        VARCHAR NOT NULL,
    category    varchar not null references categories (category) on delete cascade,
    description varchar not null,
    coordinateX int     not null default 0,
    coordinateY int     not null default 0,
    website     varchar not null default ('None'),
    price       int     not null default 0,
    start_time  int     not null default 0,
    end_time    int     not null default 0
);
