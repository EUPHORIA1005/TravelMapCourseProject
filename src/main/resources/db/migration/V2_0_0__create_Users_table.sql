CREATE TABLE users
(
    id       serial primary key,
    username VARCHAR NOT NULL UNIQUE,
    password VARCHAR NOT NULL,
    role    VARCHAR NOT NULL DEFAULT ('ROLE_USER')
);


INSERT INTO users (id, username, password, role)
VALUES (1, 'admin', '$2y$12$jvFwuQf8TRW8zBGcnT/ogeF3A0xeWBHj.m6o4Vy0bidEvz/bYKq1W', 'ROLE_ADMIN');

INSERT INTO users (id, username, password, role)
VALUES (2, 'user', '$2y$12$jvFwuQf8TRW8zBGcnT/ogeF3A0xeWBHj.m6o4Vy0bidEvz/bYKq1W', 'ROLE_USER'); /*For test*/

