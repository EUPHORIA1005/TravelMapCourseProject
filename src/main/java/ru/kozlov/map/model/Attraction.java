package ru.kozlov.map.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Attraction {

    int id;

    @NotBlank(message = "Name is required")
    @NotNull(message = "Name is required")
    String name;

    @NotBlank(message = "Category is required")
    @NotNull(message = "Category is required")
    String category;

    @NotBlank(message = "Description is required")
    @NotNull(message = "Description is required")
    String description;

    @NotNull(message = "X coordinate can not be null")
    int coordinateX;

    @NotNull(message = "Y coordinate can not be null")
    int coordinateY;

    String website;

    @Min(0)
    int price;

    @Min(0)
    @Max(24)
    int start_time;

    @Min(0)
    @Max(24)
    int end_time;
}
