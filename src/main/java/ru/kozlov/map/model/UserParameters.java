package ru.kozlov.map.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserParameters {

    List<String> categoriesList;
    @Min(0)
    int budget;
    int coordinateX;
    int coordinateY;
}
