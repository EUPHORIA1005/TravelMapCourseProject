package ru.kozlov.map.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ru.kozlov.map.model.Attraction;

import java.util.List;
import java.util.Optional;

@Mapper
public interface AttractionRepository {

    @Select("Select * from attractions where category = #{category}")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "category", property = "category"),
            @Result(column = "description", property = "description"),
            @Result(column = "coordinateX", property = "coordinateX"),
            @Result(column = "coordinateY", property = "coordinateY"),
            @Result(column = "website", property = "website"),
            @Result(column = "price", property = "price"),
            @Result(column = "start_time", property = "start_time"),
            @Result(column = "end_time", property = "end_time")
    })
    List<Attraction> findAllByCategory(String category);

    @Select("Select * from attractions where category in (Select category from categories where parent_id = #{parentId})")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "category", property = "category"),
            @Result(column = "description", property = "description"),
            @Result(column = "coordinateX", property = "coordinateX"),
            @Result(column = "coordinateY", property = "coordinateY"),
            @Result(column = "website", property = "website"),
            @Result(column = "price", property = "price"),
            @Result(column = "start_time", property = "start_time"),
            @Result(column = "end_time", property = "end_time")
    })
    List<Attraction> findAllByParentId(int parentId);

    @Insert("INSERT INTO attractions (name, category, description, coordinateX, coordinateY, website, price, start_time, end_time) " +
            "VALUES (#{name}, #{category}, #{description}, #{coordinateX}, #{coordinateY}, #{website}, #{price}, #{start_time}, #{end_time})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void insert(Attraction attraction);

    @Select("SELECT * FROM attractions")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "category", property = "category"),
            @Result(column = "description", property = "description"),
            @Result(column = "coordinateX", property = "coordinateX"),
            @Result(column = "coordinateY", property = "coordinateY"),
            @Result(column = "website", property = "website"),
            @Result(column = "price", property = "price"),
            @Result(column = "start_time", property = "start_time"),
            @Result(column = "end_time", property = "end_time")
    })
    List<Attraction> findAll();

    @Select("SELECT * FROM attractions WHERE id = #{id}")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "category", property = "category"),
            @Result(column = "description", property = "description"),
            @Result(column = "coordinateX", property = "coordinateX"),
            @Result(column = "coordinateY", property = "coordinateY"),
            @Result(column = "website", property = "website"),
            @Result(column = "price", property = "price"),
            @Result(column = "start_time", property = "start_time"),
            @Result(column = "end_time", property = "end_time")
    })
    Optional<Attraction> findById(int id);

    @Update("""            
            UPDATE attractions SET name = #{name}, category = #{category}, description = #{description},
            coordinateX = #{coordinateX}, coordinateY = #{coordinateY}, website = #{website},
            price = #{price}, start_time = #{start_time}, end_time = #{end_time}
            WHERE id = #{id}
            """)
    void update(Attraction attraction);

    @Delete("DELETE FROM attractions WHERE id = #{id}")
    void delete(int id);
}
