package ru.kozlov.map.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.Optional;

@Mapper
public interface CategoryRepository {

    @Select("SELECT category FROM categories WHERE category = #{category}")
    @Results(value = {
            @Result(column = "category", property = "category")
    })
    Optional<String> findByName(String category);

    @Select("SELECT id FROM categories WHERE category = #{category}")
    @Result(column = "id", property = "id")
    Optional<Integer> findIdByCategory(String category);

    @Select("SELECT parent_id FROM categories WHERE category = #{category}")
    @Result(column = "parent_id", property = "parent_id")
    Optional<Integer> findParentIdByCategory(String category);

}
