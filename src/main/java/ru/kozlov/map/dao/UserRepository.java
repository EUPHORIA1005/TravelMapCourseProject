package ru.kozlov.map.dao;


import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ru.kozlov.map.model.User;

import java.util.List;
import java.util.Optional;

@Mapper
public interface UserRepository {

    @Select("Select * from users where id = #{id}")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "username", property = "username"),
            @Result(column = "password", property = "password")})
    Optional<User> findById(int id);

    @Select("Select * from users")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "username", property = "username"),
            @Result(column = "password", property = "password")})
    List<User> findAll();

    @Insert("INSERT INTO users (id, username, password, role) VALUES (#{id},#{username}, #{password}, #{role})")
    void insert(User user);

    @Select("Select * from users where username = #{username}")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "username", property = "username"),
            @Result(column = "password", property = "password"),
            @Result(column = "role", property = "role")})
    Optional<User> findByUserName(String username);

    @Update("UPDATE users SET id = #{id}, username = #{username}, password = #{password} WHERE username = #{username}")
    void update(User user);

    @Delete("DELETE FROM users WHERE id = #{id}")
    void delete(int id);
}
