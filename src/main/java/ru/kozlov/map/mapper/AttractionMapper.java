package ru.kozlov.map.mapper;


import org.springframework.jdbc.core.RowMapper;
import ru.kozlov.map.model.Attraction;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AttractionMapper implements RowMapper<Attraction> {

    @Override
    public Attraction mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Attraction(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getString("category"),
                rs.getString("description"),
                rs.getInt("coordinateX"),
                rs.getInt("coordinateY"),
                rs.getString("website"),
                rs.getInt("price"),
                rs.getInt("start_time"),
                rs.getInt("end_time")
        );
    }
}