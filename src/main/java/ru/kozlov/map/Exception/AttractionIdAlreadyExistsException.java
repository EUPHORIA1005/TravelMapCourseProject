package ru.kozlov.map.Exception;

public class AttractionIdAlreadyExistsException extends RuntimeException {

    public AttractionIdAlreadyExistsException() {
        super("Attraction already exists");
    }
}