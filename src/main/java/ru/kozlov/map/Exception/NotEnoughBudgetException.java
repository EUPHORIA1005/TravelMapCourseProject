package ru.kozlov.map.Exception;

public class NotEnoughBudgetException extends RuntimeException {

    public NotEnoughBudgetException() {
        super("Not enough budget for any attractions with categories exists");
    }
}