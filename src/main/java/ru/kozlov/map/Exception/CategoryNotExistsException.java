package ru.kozlov.map.Exception;

public class CategoryNotExistsException extends RuntimeException {

    public CategoryNotExistsException() {
        super("Category for your attraction not found");
    }
}