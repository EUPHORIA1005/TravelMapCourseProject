package ru.kozlov.map.Exception;

public class AttractionIdNotFoundException extends RuntimeException {

    public AttractionIdNotFoundException() {
        super("Attraction not found");
    }
}