package ru.kozlov.map.Exception;

import lombok.Data;

@Data
public class ErrorResponse {

    private String error;
}
