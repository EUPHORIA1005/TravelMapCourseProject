package ru.kozlov.map.Exception;

public class AttractionsWithSuchCategoryNotExist extends RuntimeException {

    public AttractionsWithSuchCategoryNotExist() {
        super("Attraction with such category not found");
    }
}