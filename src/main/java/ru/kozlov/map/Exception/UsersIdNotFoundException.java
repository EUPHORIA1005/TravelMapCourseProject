package ru.kozlov.map.Exception;

public class UsersIdNotFoundException extends RuntimeException {

    public UsersIdNotFoundException() {
        super("User not found");
    }
}
