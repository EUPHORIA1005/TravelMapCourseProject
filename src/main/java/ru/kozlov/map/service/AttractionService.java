package ru.kozlov.map.service;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.kozlov.map.Exception.AttractionIdAlreadyExistsException;
import ru.kozlov.map.Exception.AttractionIdNotFoundException;
import ru.kozlov.map.Exception.AttractionsWithSuchCategoryNotExist;
import ru.kozlov.map.Exception.CategoryNotExistsException;
import ru.kozlov.map.Exception.NotEnoughBudgetException;
import ru.kozlov.map.dao.AttractionRepository;
import ru.kozlov.map.dao.CategoryRepository;
import ru.kozlov.map.model.Attraction;
import ru.kozlov.map.model.UserParameters;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class AttractionService {

    private final AttractionRepository repository;
    private final CategoryRepository categoryRepository;
    @Autowired
    private final SqlSessionFactory sqlSessionFactory;

    @Cacheable(value = "attractions")
    public Attraction findById(int id) {
        return repository.findById(id).orElse(null);
    }

    public List<Attraction> findAll() {
        return repository.findAll();
    }

    public void insertBatch(List<Attraction> attractionList) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            AttractionRepository mapper = sqlSession.getMapper(AttractionRepository.class);
            for (Attraction attraction : attractionList) {

                if (repository.findById(attraction.getId()).isPresent())
                    throw new AttractionIdAlreadyExistsException();

                if (categoryRepository.findByName(attraction.getCategory()).isEmpty())
                    throw new CategoryNotExistsException();

                mapper.insert(attraction);
            }
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    public void updateBatch(List<Attraction> attractionList) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            AttractionRepository mapper = sqlSession.getMapper(AttractionRepository.class);
            for (Attraction attraction : attractionList) {

                if (repository.findById(attraction.getId()).isEmpty())
                    throw new AttractionIdNotFoundException();

                if (categoryRepository.findByName(attraction.getCategory()).isEmpty())
                    throw new CategoryNotExistsException();

                mapper.update(attraction);
            }
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    @CacheEvict("attractions")
    public void deleteBatch(List<Integer> attractionIdList) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            AttractionRepository mapper = sqlSession.getMapper(AttractionRepository.class);
            for (int attractionId : attractionIdList) {

                if (repository.findById(attractionId).isEmpty())
                    throw new AttractionIdNotFoundException();

                mapper.delete(attractionId);
            }
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    private List<List<Attraction>> getListOfAttractionsByCategories(List<String> categoriesList) {
        List<List<Attraction>> listOfAttractionsByCategories = new ArrayList<>();

        for (String category : categoriesList) {

            List<Attraction> attractionsByCategory = new ArrayList<>();

            if (categoryRepository.findParentIdByCategory(category).
                    orElseThrow(CategoryNotExistsException::new) == 0) {
                int parentCategoryId = categoryRepository.findIdByCategory(category).
                        orElseThrow(AttractionsWithSuchCategoryNotExist::new);
                attractionsByCategory = repository.findAllByParentId(parentCategoryId);
            } else
                attractionsByCategory = repository.findAllByCategory(category);

            if (attractionsByCategory.size() == 0)
                throw new AttractionsWithSuchCategoryNotExist();

            listOfAttractionsByCategories.add(attractionsByCategory);
        }
        return listOfAttractionsByCategories;
    }

    private List<List<Attraction>> getPossibleRoutesList(List<String> categoriesList) {
        List<List<Attraction>> listOfAttractionsByCategories = getListOfAttractionsByCategories(categoriesList);

        List<ImmutableSet<Attraction>> routesList = new ArrayList<>();
        for (List<Attraction> route : listOfAttractionsByCategories)
            routesList.add(ImmutableSet.copyOf(route));

        var permutationsSet = Sets.cartesianProduct(routesList);

        return new ArrayList<>(permutationsSet.stream().toList());
    }

    private int getTotalPrice(List<Attraction> route) {
        return route.stream().mapToInt(Attraction::getPrice).sum();
    }

    //Distance from user's coordinates to last attraction (Through every attraction) via manhattan distance
    private int getTotalDistance(List<Attraction> route, int coordinateX, int coordinateY) {
        int distance = Math.abs(route.get(0).getCoordinateX() - coordinateX)
                + Math.abs(route.get(0).getCoordinateY() - coordinateY);

        for (int i = 0; i < route.size() - 1; i++) {
            distance = distance + Math.abs(route.get(i).getCoordinateX() - route.get(i + 1).getCoordinateX())
                    + Math.abs(route.get(0).getCoordinateY() - route.get(i + 1).getCoordinateY());
        }
        return distance;
    }

    private boolean isRouteInsideAllWorkingHours(List<Attraction> route) {
        boolean isRouteInsideAllWorkingHours = true;
        for (int i = 0; i < route.size() - 1; i++) {
            if (route.get(i).getEnd_time() >= route.get(i + 1).getStart_time() &
                    route.get(i).getStart_time() < route.get(i).getEnd_time()) {
                isRouteInsideAllWorkingHours = false;
                break;
            }
        }
        return isRouteInsideAllWorkingHours;
    }


    public List<Attraction> getOptimalRoute(UserParameters userParameters) {
        List<List<Attraction>> possibleRoutesList = getPossibleRoutesList(userParameters.getCategoriesList());

        List<Attraction> optimalRoute = possibleRoutesList.get(0);
        int optimalPrice = getTotalPrice(possibleRoutesList.get(0));
        int optimalDistance = getTotalDistance(possibleRoutesList.get(0), userParameters.getCoordinateX(), userParameters.getCoordinateY());

        for (List<Attraction> route : possibleRoutesList) {
            int routePrice = getTotalPrice(route);
            if (routePrice > userParameters.getBudget()) {
                continue;
            }
            int routeDistance = getTotalDistance(route, userParameters.getCoordinateX(), userParameters.getCoordinateY());

            if (routeDistance <= optimalDistance & routePrice <= optimalPrice & isRouteInsideAllWorkingHours(route)) {
                optimalRoute = route;
                optimalPrice = routePrice;
                optimalDistance = routeDistance;
            }
        }

        if (optimalPrice > userParameters.getBudget())
            throw new NotEnoughBudgetException();
        return optimalRoute;
    }
}
