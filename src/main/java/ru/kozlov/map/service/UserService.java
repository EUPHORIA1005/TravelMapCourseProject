package ru.kozlov.map.service;

import lombok.AllArgsConstructor;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.kozlov.map.Exception.UserAlreadyExistsException;
import ru.kozlov.map.Exception.UsersIdNotFoundException;
import ru.kozlov.map.dao.UserRepository;
import ru.kozlov.map.model.User;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository repository;
    @Autowired
    private final SqlSessionFactory sqlSessionFactory;


    public List<User> findAll() {
        return repository.findAll();
    }

    @Cacheable("users")
    public User findById(int id) {
        return repository.findById(id).orElse(null);
    }

    public void insertBatch(List<User> userList) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            UserRepository mapper = sqlSession.getMapper(UserRepository.class);
            for (User user : userList) {
                if (repository.findById(user.getId()).isPresent())
                    throw new UserAlreadyExistsException();
                mapper.insert(user);
            }
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    public void updateBatch(List<User> userList) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            UserRepository mapper = sqlSession.getMapper(UserRepository.class);
            for (User user : userList) {
                if (repository.findById(user.getId()).isEmpty())
                    throw new UsersIdNotFoundException();
                mapper.update(user);
            }
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    @CacheEvict("users")
    public void deleteBatch(List<Integer> userIdList) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            UserRepository mapper = sqlSession.getMapper(UserRepository.class);
            for (int userId : userIdList) {
                if (repository.findById(userId).isEmpty())
                    throw new UsersIdNotFoundException();
                mapper.delete(userId);
            }
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }
}
