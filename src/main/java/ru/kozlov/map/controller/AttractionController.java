package ru.kozlov.map.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kozlov.map.Exception.AttractionIdNotFoundException;
import ru.kozlov.map.model.Attraction;
import ru.kozlov.map.model.UserParameters;
import ru.kozlov.map.service.AttractionService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("/attractions/")
@RequiredArgsConstructor
@Validated
@Api("AttractionController")
public class AttractionController {

    private final AttractionService attractionService;

    @GetMapping
    public List<Attraction> findAllAttractions() {
        return attractionService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<Attraction> getAttractionById(@PathVariable("id") int id) {
        Attraction attraction = attractionService.findById(id);
        if (attraction == null) {
            throw new AttractionIdNotFoundException();
        } else {
            return new ResponseEntity<>(attraction, HttpStatus.OK);
        }
    }

    @PostMapping()
    public ResponseEntity<?> insertBatchAttraction(@RequestBody @NotEmpty(message = "List cant be empty") List<@Valid Attraction> attractionList) {
        attractionService.insertBatch(attractionList);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<?> updateBatchAttraction(@RequestBody @NotEmpty(message = "List cant be empty") List<@Valid Attraction> attractionList) {
        attractionService.updateBatch(attractionList);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<?> deleteAttraction(@RequestBody @NotEmpty(message = "List cant be empty") List<Integer> attractionIdList) {
        attractionService.deleteBatch(attractionIdList);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("optimalRoute")
    public ResponseEntity<?> findOptimalRoute(@RequestBody @Valid UserParameters userParameters) {
        return new ResponseEntity<>(attractionService.getOptimalRoute(userParameters), HttpStatus.OK);
    }
}
