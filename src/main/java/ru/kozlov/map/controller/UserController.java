package ru.kozlov.map.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kozlov.map.Exception.UsersIdNotFoundException;
import ru.kozlov.map.model.User;
import ru.kozlov.map.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("/users/")
@RequiredArgsConstructor
@Api("UserController")
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<User> findAllUsers() {
        return userService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") int id) {
        User user = userService.findById(id);
        if (user == null) {
            throw new UsersIdNotFoundException();
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @PostMapping()
    public ResponseEntity<?> insertBatchUsers(@RequestBody @NotEmpty(message = "List cant be empty") List<@Valid User> userList) {
        userService.insertBatch(userList);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<?> updateBatchUsers(@RequestBody @NotEmpty(message = "List cant be empty") List<@Valid User> courseList) {
        userService.updateBatch(courseList);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<?> deleteBatchUsers(@RequestBody @NotEmpty(message = "List cant be empty") List<Integer> courseIdList) {
        userService.deleteBatch(courseIdList);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
