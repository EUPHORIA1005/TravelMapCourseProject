package ru.kozlov.map.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;
import ru.kozlov.map.MapApplicationTests;
import ru.kozlov.map.mapper.AttractionMapper;
import ru.kozlov.map.model.Attraction;
import ru.kozlov.map.model.UserParameters;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class AttractionControllerTest extends MapApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final List<Attraction> attractionList = new ArrayList<>();

    private String jsonList;


    @BeforeEach
    void tearDown() {
        cleanAndMigrate();
    }

    @BeforeEach
    void setUpAttractions() {
        Attraction testAttraction1 = new Attraction(1, "attractionname1", "cafe", "description1", 1, 1, "", 1, 1, 20);
        Attraction testAttraction2 = new Attraction(2, "attractionname2", "museum", "description2", 2, 2, "", 2, 9, 22);
        Attraction testAttraction3 = new Attraction(3, "attractionname3", "cinema", "description3", 3, 3, "", 3, 5, 15);
        attractionList.add(testAttraction1);
        attractionList.add(testAttraction2);
        attractionList.add(testAttraction3);
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    void postAttractionList() throws Exception {
        jsonList = objectMapper.writeValueAsString(attractionList);

        mockMvc.perform(post("/attractions/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andDo(print());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "INSERT INTO attractions (name, category, description, coordinateX, coordinateY, website, price, start_time, end_time)" +
            " values('attractionname1', 'cafe', 'description1', 1, 1, '', 1, 1, 2)")
    void getAttractionByIdNotFound() throws Exception {
        mockMvc.perform(get("/attractions/" + 2))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"Attraction not found\"}"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void findAllAttractions() throws Exception {
        postAttractionList();

        mockMvc.perform(get("/attractions/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(jsonList));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getAttractionByIdSuccess() throws Exception {
        postAttractionList();
        Attraction testAttraction = new Attraction(1, "attractionname1", "cafe", "description1", 1, 1, "", 1, 1, 20);
        String expectedContent = objectMapper.writeValueAsString(testAttraction);

        mockMvc.perform(get("/attractions/" + 1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        Attraction dbAttraction = jdbcTemplate.queryForObject("Select * from attractions where id = 1", new AttractionMapper());
        assertEquals(testAttraction, dbAttraction);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void insertBatchAttractionAlreadyExists() throws Exception {
        postAttractionList();

        mockMvc.perform(post("/attractions/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andDo(print())
                .andExpect(content().string("{\"error\":\"Attraction already exists\"}"));
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        assertEquals(attractionList, dbAttractionList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void insertBatchAttractionSuccess() throws Exception {
        String jsonList = objectMapper.writeValueAsString(attractionList);
        mockMvc.perform(post("/attractions/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andExpect(status().isOk());
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        assertEquals(attractionList, dbAttractionList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void insertBatchAttractionNotValid() throws Exception {
        attractionList.get(2).setPrice(-100);
        String jsonList = objectMapper.writeValueAsString(attractionList);
        NestedServletException exception = assertThrows(
                NestedServletException.class,
                () -> {
                    mockMvc.perform(post("/attractions/").contentType(MediaType.APPLICATION_JSON)
                            .content(jsonList));
                }
        );
        Assertions.assertEquals("insertBatchAttraction.attractionList[2].price: must be greater than or equal to 0", exception.getRootCause().getMessage());
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        List<Attraction> nullList = new ArrayList<>(); // empty
        assertEquals(nullList, dbAttractionList);
    }


    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateBatchAttractionSuccess() throws Exception {
        postAttractionList();

        attractionList.get(0).setName("New name");
        String updatedJsonList = objectMapper.writeValueAsString(attractionList);
        mockMvc.perform(put("/attractions/").contentType(MediaType.APPLICATION_JSON)
                .content(updatedJsonList))
                .andExpect(status().isOk());
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        assertEquals(attractionList, dbAttractionList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateBatchAttractionIdNotFound() throws Exception {
        postAttractionList();

        attractionList.get(0).setId(1337);
        String updatedJsonList = objectMapper.writeValueAsString(attractionList);
        mockMvc.perform(put("/attractions/").contentType(MediaType.APPLICATION_JSON)
                .content(updatedJsonList))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"Attraction not found\"}"));

        //setting id back to normal -> asserting db did not change
        attractionList.get(0).setId(1);
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        assertEquals(attractionList, dbAttractionList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteAttractionSuccess() throws Exception {
        postAttractionList();

        List<Integer> deleteList = new ArrayList<>();
        deleteList.add(1);
        deleteList.add(2);
        String jsonDeleteList = objectMapper.writeValueAsString(deleteList);
        mockMvc.perform(delete("/attractions/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeleteList))
                .andExpect(status().isOk());

        attractionList.remove(0);
        attractionList.remove(0);
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        assertEquals(attractionList, dbAttractionList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteAttractionIdNotFound() throws Exception {
        postAttractionList();

        List<Integer> deleteList = new ArrayList<>();
        deleteList.add(1337);
        String jsonDeleteList = objectMapper.writeValueAsString(deleteList);
        mockMvc.perform(delete("/attractions/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeleteList))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"Attraction not found\"}"));
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        assertEquals(attractionList, dbAttractionList);
    }

    //Security tests

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void postAttractionsNotAuthorized() throws Exception {
        String jsonList = objectMapper.writeValueAsString(attractionList);

        mockMvc.perform(post("/attractions/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andExpect(status().isForbidden());
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        List<Attraction> nullList = new ArrayList<>();
        assertEquals(nullList, dbAttractionList);
    }

    @Test
    @WithAnonymousUser
    void postAttractionsNotAuthenticated() throws Exception {
        String jsonList = objectMapper.writeValueAsString(attractionList);

        mockMvc.perform(post("/attractions/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andExpect(status().isUnauthorized());
        List<Attraction> dbAttractionList = jdbcTemplate.query("Select * from attractions", new AttractionMapper());
        List<Attraction> nullList = new ArrayList<>();
        assertEquals(nullList, dbAttractionList);
    }


    //Optimal route test

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getOptimalRouteCategoryNotFound() throws Exception {
        postAttractionList();

        List<String> categoriesList = new ArrayList<>();
        categoriesList.add("cafe");
        categoriesList.add("notCinema");

        String jsonUserParameters = objectMapper.writeValueAsString(new UserParameters(categoriesList, 4, 1, 1));


        mockMvc.perform(get("/attractions/optimalRoute").contentType(MediaType.APPLICATION_JSON)
                .content(jsonUserParameters))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"Category for your attraction not found\"}"));

    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getOptimalRouteNotEnoughBudget() throws Exception {
        postAttractionList();

        List<String> categoriesList = new ArrayList<>();
        categoriesList.add("cafe");
        categoriesList.add("cinema");

        String jsonUserParameters = objectMapper.writeValueAsString(new UserParameters(categoriesList, 0, 1, 1));

        mockMvc.perform(get("/attractions/optimalRoute").contentType(MediaType.APPLICATION_JSON)
                .content(jsonUserParameters))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"Not enough budget for any attractions with categories exists\"}"));

    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getOptimalRouteSuccess() throws Exception {
        postAttractionList();

        List<String> categoriesList = new ArrayList<>();
        categoriesList.add("cafe");
        categoriesList.add("museum");
        List<Attraction> expectedRoute = new ArrayList<>();
        expectedRoute.add(attractionList.get(0));
        expectedRoute.add(attractionList.get(1));

        String jsonExpectedRoute = objectMapper.writeValueAsString(expectedRoute);
        String jsonUserParameters = objectMapper.writeValueAsString(new UserParameters(categoriesList, 10, 1, 1));

        mockMvc.perform(get("/attractions/optimalRoute").contentType(MediaType.APPLICATION_JSON)
                .content(jsonUserParameters))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonExpectedRoute));


    }
}