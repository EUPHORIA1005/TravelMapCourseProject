package ru.kozlov.map.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;
import ru.kozlov.map.MapApplicationTests;
import ru.kozlov.map.mapper.UserMapper;
import ru.kozlov.map.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
class UserControllerTest extends MapApplicationTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final List<User> userList = new ArrayList<>();

    private String jsonList;

    @BeforeEach
    void tearDown() {
        cleanAndMigrate();
    }

    @BeforeEach
    void setUpUsers() {
        User testUser1 = new User(3, "username1", "1", "ROLE_USER");
        User testUser2 = new User(4, "username2", "2", "ROLE_USER");
        User testUser3 = new User(5, "username3", "3", "ROLE_USER");
        userList.add(testUser1);
        userList.add(testUser2);
        userList.add(testUser3);
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    void postUserList() throws Exception {
        jsonList = objectMapper.writeValueAsString(userList);

        mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andDo(print());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getUserByIdNotFound() throws Exception {
        mockMvc.perform(get("/users/" + 1337))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"User not found\"}"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void findAllUsers() throws Exception {
        postUserList();
        userList.add(0, new User(1, "admin", "$2y$12$jvFwuQf8TRW8zBGcnT/ogeF3A0xeWBHj.m6o4Vy0bidEvz/bYKq1W", "ROLE_ADMIN"));
        userList.add(1, new User(2, "user", "$2y$12$jvFwuQf8TRW8zBGcnT/ogeF3A0xeWBHj.m6o4Vy0bidEvz/bYKq1W", "ROLE_USER"));
        jsonList = objectMapper.writeValueAsString(userList);

        mockMvc.perform(get("/users/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(jsonList));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getUserByIdSuccess() throws Exception {
        postUserList();
        User testUser = new User(3, "username1", "1", "ROLE_USER");
        String expectedContent = objectMapper.writeValueAsString(testUser);

        mockMvc.perform(get("/users/" + 3))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        User dbUser = jdbcTemplate.queryForObject("Select * from users where id = 3", new UserMapper());
        assertEquals(testUser, dbUser);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void insertBatchUserAlreadyExists() throws Exception {
        postUserList();

        mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andDo(print())
                .andExpect(content().string("{\"error\":\"User already exists\"}"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void insertBatchUserSuccess() throws Exception {
        String jsonList = objectMapper.writeValueAsString(userList);
        mockMvc.perform(post("/users/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonList))
                .andExpect(status().isOk());
        List<User> dbUserList = jdbcTemplate.query("Select * from users where id > 2", new UserMapper());
        assertEquals(userList, dbUserList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateBatchUserSuccess() throws Exception {
        postUserList();

        userList.get(0).setPassword("NewName");
        String updatedJsonList = objectMapper.writeValueAsString(userList);
        mockMvc.perform(put("/users/").contentType(MediaType.APPLICATION_JSON)
                .content(updatedJsonList))
                .andExpect(status().isOk());
        List<User> dbUserList = jdbcTemplate.query("Select * from users where id > 2", new UserMapper());
        assertEquals(userList, dbUserList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateBatchUserIdNotFound() throws Exception {
        postUserList();
        List<User> dbUserListBefore = jdbcTemplate.query("Select * from users where id > 2", new UserMapper());

        userList.get(0).setId(1337);
        String updatedJsonList = objectMapper.writeValueAsString(userList);
        mockMvc.perform(put("/users/").contentType(MediaType.APPLICATION_JSON)
                .content(updatedJsonList))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"User not found\"}"));

        //setting id back to normal -> asserting db did not change
        List<User> dbUserListAfter = jdbcTemplate.query("Select * from users where id > 2", new UserMapper());
        assertEquals(dbUserListBefore, dbUserListAfter);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteUserSuccess() throws Exception {
        postUserList();

        List<Integer> deleteList = new ArrayList<>();
        deleteList.add(3);
        deleteList.add(4);
        String jsonDeleteList = objectMapper.writeValueAsString(deleteList);
        mockMvc.perform(delete("/users/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeleteList))
                .andExpect(status().isOk());

        userList.remove(0);
        userList.remove(0);
        List<User> dbUserList = jdbcTemplate.query("Select * from users where (users.id > 2)", new UserMapper());
        assertEquals(userList, dbUserList);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteUserIdNotFound() throws Exception {
        postUserList();

        List<Integer> deleteList = new ArrayList<>();
        deleteList.add(1337);
        String jsonDeleteList = objectMapper.writeValueAsString(deleteList);
        mockMvc.perform(delete("/users/").contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeleteList))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"error\":\"User not found\"}"));
        List<User> dbUserList = jdbcTemplate.query("Select * from users where id > 2", new UserMapper());
        assertEquals(userList, dbUserList);
    }

}